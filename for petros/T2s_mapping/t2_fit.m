%function t2_fit

close all;
clear all;

global noise_mean

noise_mean = 0;
noise_std = 0;

ima_count = 0;
% Retrieve first image from user directory
[fname, pname] = uigetfile( ...
     {  '*.*',  'All Files (*.*)'; ...
        '*.dcm','DICOM-files (*.dcm)'; ...
        '*.nema','NEMA-files (*.nema)'}, ...    
        'Open Image Files');

% if no file was chosen -> back
if fname==0
    return;
end

fname=lower(fname);   
cd(pname);

% Retrieve all images from the directory
names=dir;
for n = 1:(length(names))
   if (names(n).isdir)
       continue;
   end;
   ima_count=ima_count+1;
   try
       info=dicominfo(names(n).name);
       ima_orig(:,:,ima_count)=dicomread(info);
       TE(ima_count) = info.EchoTime;
   catch  
       ima_count=ima_count-1;
   end
end    

ima_data = double(ima_orig);

lin = size(ima_data,1);
col = size(ima_data,2);

% display all images
images_in_row = ceil(sqrt(ima_count));
images_in_col = ceil(ima_count/images_in_row);
figure('Name','Original images');
for i=1:ima_count
    subplot(images_in_row,images_in_col,i);subimage(ima_data(:,:,i),[0 2000]);
    axis off;
end

xdata= TE;

prompt = {'Enter number of parameters for fit procedure'};
dlg_title = 'Enter Value';
num_lines = 1;
exp_def = {'2'};
exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
num_fit_para = str2num(cell2mat(exp_param));
clear prompt dlg_title num_lines exp_def exp_param;

% start values for fit procedure
if (num_fit_para == 2)
    prompt = {'S_0','T2: [ms]'};
    dlg_title = 'Enter start parameters';
    num_lines = 1;
    exp_def = {'1000', '1.0'};
    exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
elseif (num_fit_para == 3)
    prompt = {'S_0','T2: [ms]','noise'};
    dlg_title = 'Enter start parameters';
    num_lines = 1;
    exp_def = {'1000', '1.0', '100'};
    exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
else
    errordlg(lasterr,'Error: incorrect input data','modal');
    uiwait;
    close all;
end
for i=1:length(exp_param)
    start_par(i) = str2num(cell2mat(exp_param(i)));
end
clear prompt dlg_title num_lines exp_def exp_param;


%select ROI for image noise  
figure;
imshow(ima_data(:,:,1),[0 500], 'InitialMagnification','fit');
title('Select ROI for background noise calculation');
ima_noise = imcrop;
close;
noise_mean = mean2(ima_noise);
noise_std = std2(ima_noise);


% select ROI for mapping 
figure;
imshow(ima_data(:,:,1),[0 2000], 'InitialMagnification','fit');
title('Crop images to the region of interest');
[ima_crop,roi_map] = imcrop;
for i=1:ima_count
    ima_crop(:,:,i) = imcrop(ima_data(:,:,i),roi_map);        
end


% display croped images
figure('Name','Croped images');
for i=1:ima_count
    subplot(images_in_row,images_in_col,i);subimage(ima_crop(:,:,i),[0 2000]);
    axis off;
end

options = optimset('Algorithm', 'trust-region-reflective');
scrsz = get(0,'ScreenSize');
nroi=0;
while (nroi<100)
    figure;
    imshow(ima_crop(:,:,2),[0 2000], 'InitialMagnification','fit');
    title('Select ROI for T2 calculation');
    ROI_mask=roipoly;
    if (isempty(ROI_mask))
        errordlg(lasterr,'Error: incorrect input data','modal');
        break;
    end;
    
    ROI_vec=find(ROI_mask~=0);
    for n=1:ima_count
        ima_tmp = ima_crop(:,:,n);
        ydata(n) = mean(ima_tmp(ROI_vec));
    end
    SST_tmp = (ydata - mean(ydata)).^2;
    SST = sum(SST_tmp);
    
    if (ydata(1) > noise_mean + 3*noise_std)
        if (num_fit_para == 2)
            lb = [0 0];
            ub = [10000 10];
            [calc_par,resnorm,~,~,~]=lsqcurvefit('t2_fit_2par',start_par,xdata,ydata,lb,ub,options);      % for 2-parameter fit
            M0 = calc_par(1);
            T2 = calc_par(2);
            Noise = noise_mean;
        elseif (num_fit_para == 3)
            lb = [0 0 0];
            ub = [10000 10 500];
            [calc_par,resnorm,~,~,~]=lsqcurvefit('t2_fit_3par',start_par,xdata,ydata,lb,ub,options);     % for 3-parameter fit
            M0 = calc_par(1);
            T2 = calc_par(2);
            Noise = calc_par(3);
        else
            errordlg(lasterr,'Error: incorrect input data','modal');
            uiwait;
            close all;
        end
        GoodnessOfFit_SSE = resnorm;
        GoodnessOfFit_Rsquare = 1-resnorm/SST;
        GoodnessOfFit_RsquareAdj = 1-resnorm*(ima_count-1)/(SST*(ima_count-length(calc_par)));
        GoodnessOfFit_RMSE = sqrt(resnorm/(ima_count-length(calc_par)));
    else
        T2 = 0;
        M0 = 0;
        Noise = 0;
        GoodnessOfFit_resnorm = Inf;
        GoodnessOfFit_RMSE = Inf;
        GoodnessOfFit_Rsquare = 0;
        GoodnessOfFit_RsquareAdj = 0;
    end
       
    t = 0:0.001:xdata(ima_count)+0.01;
    figure('Position',[10 scrsz(4)/3 scrsz(4)/2 scrsz(4)/2]);
   
    plot(xdata,ydata,'*');
    hold on
    plot(t, Noise + M0*exp(-t/T2), '--r');
    legend( 'Data', ['T2 = ' num2str(T2,'%5.2f') ' ms']);
    text(xdata(round(ima_count/2)),(ydata(1)+ydata(2))/2,['Goodness of Fit = ' num2str(GoodnessOfFit_Rsquare, '%6.4f')]);
    title('Data fitting');
    hold off
    
    nroi=nroi+1;
end

%end