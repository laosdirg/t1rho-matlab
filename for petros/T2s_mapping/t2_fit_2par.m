function [F,J] = t2_fit_2par(x,xdata)
nx = length(xdata);
i=1:nx;

global noise_mean

noise_value = noise_mean;

% fit function
F(i) = noise_value + x(1) * exp(-xdata(i)/x(2));

% derivative for x1-variable: dF/dx(1)
Gx1(i) = exp(-xdata(i)/x(2)); 

% derivative for x2-variable: dF/dx(2)
Gx2(i) =  x(1) .* xdata(i)/(x(2).^2) .*exp( -xdata(i) / x(2) );

Gx = [Gx1(i); Gx2(i)];
if nargout > 1
    J = Gx.';
end