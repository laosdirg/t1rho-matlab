function [F,J] = t2_fit_3par(x,xdata)
nx = length(xdata);
i=1:nx;

% fit function
F(i) = x(3) + x(1) * exp(-xdata(i)/x(2));

% derivative for x1-variable: dF/dx(1)
Gx1(i) = exp(-xdata(i)/x(2));

% derivative for x2-variable: dF/dx(3)
Gx2(i) = x(1) * (xdata(i)/(x(2).^2)) .* exp(-xdata(i)/x(2)) ;

% derivative for x3-variable: dF/dx(3)
Gx3(i) = 1;


Gx = [Gx1(i); Gx2(i); Gx3(i)];
if nargout > 1
    J = Gx.';
end