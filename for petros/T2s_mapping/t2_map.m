function t2_map
close all;
clear all;

global noise_mean

noise_mean = 0;
noise_std = 0;

% Retrieve first image from user directory
[fname, pname] = uigetfile( ...
     {  '*.*',  'All Files (*.*)'; ...
        '*.dcm','DICOM-files (*.dcm)'; ...
        '*.nema','NEMA-files (*.nema)'}, ...    
        'Open Image Files');

% if no file was chosen -> back
if fname==0
    return;
end

fname=lower(fname);   
cd(pname);

ima_count = 0;
% Retrieve all images from the directory
names=dir;
for n = 1:(length(names))
   if (names(n).isdir)
       continue;
   end;
   ima_count=ima_count+1;
   try
       info=dicominfo(names(n).name);
       ima_orig(:,:,ima_count)=dicomread(info);
       TE(ima_count) = info.EchoTime;
catch  
       ima_count=ima_count-1;
   end
end    

ima_data = double(ima_orig);

lin = size(ima_data,1);
col = size(ima_data,2);

% display all images
images_in_row = ceil(sqrt(ima_count));
images_in_col = ceil(ima_count/images_in_row);
figure('Name','Original images');
for i=1:ima_count
    subplot(images_in_row,images_in_col,i);subimage(ima_data(:,:,i),[0 2000]);
    axis off;
end

xdata = TE;

prompt = {'Enter number of parameters for fit procedure'};
dlg_title = 'Enter Value';
num_lines = 1;
exp_def = {'2'};
exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
num_fit_para = str2num(cell2mat(exp_param));
clear prompt dlg_title num_lines exp_def exp_param;

% start values for fit procedure
if (num_fit_para == 2)
    prompt = {'S_0','T2: [ms]'};
    dlg_title = 'Enter start parameters';
    num_lines = 1;
    exp_def = {'1000', '1.0'};
    exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
elseif (num_fit_para == 3)
    prompt = {'S_0','T2: [ms]','noise'};
    dlg_title = 'Enter start parameters';
    num_lines = 1;
    exp_def = {'1000', '1.0', '100'};
    exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
else
    errordlg(lasterr,'Error: incorrect input data','modal');
    uiwait;
    close all;
end
for i=1:length(exp_param)
    start_par(i) = str2num(cell2mat(exp_param(i)));
end
clear prompt dlg_title num_lines exp_def exp_param;


% select ROI for image noise  
figure;
imshow(ima_data(:,:,1),[0 500], 'InitialMagnification','fit');
title('Select ROI for background noise calculation');
ima_noise = imcrop;
close;
noise_mean = mean2(ima_noise);
noise_std = std2(ima_noise);


% select ROI for mapping 
figure;
imshow(ima_data(:,:,1),[0 2000], 'InitialMagnification','fit');
title('Crop images for mapping');
[ima_crop,roi_map] = imcrop;
close;
for i=1:ima_count
    ima_crop(:,:,i) = imcrop(ima_data(:,:,i),roi_map);        
end

figure('Name','Croped images');
for i=1:ima_count
    subplot(images_in_row,images_in_col,i);subimage(ima_crop(:,:,i),[0 2000]);
    axis off;
end

lin_crop = ceil(roi_map(4));
col_crop = ceil(roi_map(3));

M0_map = zeros(lin_crop,col_crop);
T2_map = zeros(lin_crop,col_crop);
Noise_map = zeros(lin_crop,col_crop);

SST = zeros(lin_crop,col_crop);
GoodnessOfFit_SSE = Inf(lin_crop,col_crop);
GoodnessOfFit_RMSE = Inf(lin_crop,col_crop);
GoodnessOfFit_Rsquare = zeros(lin_crop,col_crop);
GoodnessOfFit_RsquareAdj = zeros(lin_crop,col_crop);


options = optimset('Algorithm', 'trust-region-reflective');
wait = timebar('Computation in progress...','Wait...');
for i=1:lin_crop;
    for j=1:col_crop;                
        for m=1:ima_count
            ydata(m) = ima_crop(i,j,m);                                                  
        end
        % SST - sum of squares about the mean
        SST_tmp = (ydata - mean(ydata)).^2;
        SST(i,j) = sum(SST_tmp);
        clear SST_tmp;

        if (ydata(1) > (noise_mean + 3*noise_std))            
            if (num_fit_para == 2)
                lb = [0 0];
                ub = [10000 10];
                [calc_par,resnorm,~,~,output]=lsqcurvefit('t2_fit_2par',start_par,xdata,ydata,lb,ub,options);           
                M0_map(i,j) = calc_par(1);
                T2_map(i,j) = calc_par(2);
                Noise_map(i,j) = noise_mean;
                GoodnessOfFit_SSE(i,j) = resnorm; 
            elseif (num_fit_para == 3)
                lb = [0 0 0];
                ub = [10000 10 500];
                [calc_par,resnorm,~,~,output]=lsqcurvefit('t2_fit_3par',start_par,xdata,ydata,lb,ub,options);
                M0_map(i,j) = calc_par(1);
                T2_map(i,j) = calc_par(2);
                Noise_map(i,j) = calc_par(3);
                GoodnessOfFit_SSE(i,j) = resnorm;
            else
                errordlg(lasterr,'Error: incorrect input data','modal');
                uiwait;
                close all;
            end
        end
        timebar(wait,((i-1)*col_crop+j)/(lin_crop*col_crop));            
    end
end
close(wait);                                       


GoodnessOfFit_Rsquare = 1-GoodnessOfFit_SSE./SST;
GoodnessOfFit_RsquareAdj = 1-(ima_count-1)*GoodnessOfFit_SSE./(SST*(ima_count-length(calc_par)));
GoodnessOfFit_RMSE = sqrt(GoodnessOfFit_SSE/(ima_count-length(calc_par)));

T2_map(GoodnessOfFit_Rsquare>1)=0;
T2_map(GoodnessOfFit_Rsquare<0.5)=0;
T2_map(T2_map<0)=0;
T2_map(T2_map>1000)=0;

figure('Name', 'T2 Map');    
imshow(T2_map,[0 5], 'InitialMagnification','fit');  
title('T2 map');
colorbar; colormap(hsv);

figure('Name', 'Goodness of fit');  
title('Goodness of Fit');
imshow(GoodnessOfFit_Rsquare,[0 1], 'InitialMagnification','fit');   
colorbar; colormap(hsv);

[fname1,pname1] = uiputfile('*.mat','Save T2 Map');
%falls kein file ausgew�hlt -> zur�ck
if fname1==0
    return;
end
cd(pname1);
save(fname1, 'T2_map', 'GoodnessOfFit_Rsquare');


% %F�r ROI-Analyse
% alpha_rad=alpha*pi/180.0; % [rad]
% t = 0:0.001:xdata(ima_count)+0.01;
% scrsz = get(0,'ScreenSize');
% q=1;
% while q < 100
%     figure;
%     imshow(ima_crop(:,:,1),[0 2000], 'InitialMagnification', 'fit');
%     title('Select ROI for T2 calculation');    
%     roi_mask = roipoly; close;
%     if (isempty(roi_mask))
%         errordlg(lasterr,'Error: incorrect input data','modal');
%         break;
%     end;
%     roi_vec = find(roi_mask);
%     
%     for p=1:ima_count
%         ima_tmp = ima_crop(:,:,p);
%         roi_mean(p) = mean(ima_tmp(roi_vec));
%     end
%     SST_tmp = (roi_mean - mean(roi_mean)).^2;
%     SST = sum(SST_tmp);
% 
%     noise_tmp = mean(Noise_map(roi_vec));
%     m0_tmp = mean(M0_map(roi_vec));
%     t2_tmp = mean(T2_map(roi_vec));
%     
%     GoodnessOfFit_ROI_SSE = mean(GoodnessOfFit_SSE(roi_vec));
%     GoodnessOfFit_ROI_Rsquare = mean(GoodnessOfFit_Rsquare(roi_vec));
%     GoodnessOfFit_ROI_RsquareAdj = mean(GoodnessOfFit_RsquareAdj(roi_vec));
%     GoodnessOfFit_ROI_RMSE =  mean(GoodnessOfFit_RMSE(roi_vec));
%     
%     if ((GoodnessOfFit_ROI_Rsquare > 1) || (GoodnessOfFit_ROI_Rsquare < 0.5))
%         noise_tmp = 0;
%         t2_tmp = 0;
%         m0_tmp = 0;
%     end
%     
%     figure('Position',[10 scrsz(4)/3 scrsz(4)/2 scrsz(4)/2]);
%     plot(xdata,squeeze(roi_mean(1:ima_count)),'*b','LineWidth',2);
%     hold on
%     plot(t, noise_tmp + m0_tmp*sin(alpha_rad)*exp(-t/t2_tmp).*(1 - exp(-(TR-t)/T1))./(1 - exp(-t/t2_tmp).*exp(-(TR-t)/T1)*cos(alpha_rad)), ':r');
%     xlabel('Spin lock duration (ms)');
%     ylabel('Signal intensity (a.u.)');
%     legend( 'Data', ['T1_{\rho} = ' num2str(t2_tmp,'%5.2f') ' ms'])
%     text(xdata(ima_count-1),roi_mean(1),['Goodness of Fit = ' num2str(GoodnessOfFit_ROI_Rsquare, '%6.4f')]);
%     title('Data fitting');
%     hold off
%     
%     clear roi_vec roi mask;
%     q=q+1;
% end


end