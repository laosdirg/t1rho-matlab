function t1rho_fit

close all;
clear all;

global noise_mean TR T1 alpha

noise_mean = 0;
noise_std = 0;

ima_count = 0;
% Retrieve first image from user directory
[fname, pname] = uigetfile( ...
     {  '*.*',  'All Files (*.*)'; ...
        '*.dcm','DICOM-files (*.dcm)'; ...
        '*.nema','NEMA-files (*.nema)'}, ...    
        'Open Image Files');

% if no file was chosen -> back
if fname==0
    return;
end

fname=lower(fname);   
cd(pname);

% Retrieve all images from the directory
names=dir;
for n = 1:(length(names))
   if (names(n).isdir)
       continue;
   end;
   ima_count=ima_count+1;
   try
       info=dicominfo(names(n).name);
       ima_orig(:,:,ima_count)=dicomread(info);
   catch  
       ima_count=ima_count-1;
   end
end    

TR = info.RepetitionTime;
alpha = info.FlipAngle;

ima_data = double(ima_orig);

lin = size(ima_data,1);
col = size(ima_data,2);

M0_map = zeros(lin,col);
T1rho_map = zeros(lin,col);

% display all images
figure('Name','Original images');
for i=1:ima_count
    subplot(1,ima_count,i);subimage(ima_data(:,:,i),[0 500]);
    axis off;
end

if (ima_count < 3)
    errordlg(lasterr,'Error: incorrect input data','modal');
    return;
else
    switch ima_count
        case 3
            prompt = {'TSL-1, [ms]:','TSL-2: [ms]','TSL-3: [ms]'};
            dlg_title = 'Enter Values';
            num_lines = 1;
            exp_def = {'1.0', '2.0', '4.0'};
            exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
        case 4
            prompt = {'TSL-1, [ms]:','TSL-2: [ms]','TSL-3: [ms]','TSL-4: [ms]'};
            dlg_title = 'Enter Values';
            num_lines = 1;
            exp_def = {'1.0', '2.0', '4.0', '8.0'};
            exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
        case 5
            prompt = {'TSL-1, [ms]:','TSL-2: [ms]','TSL-3: [ms]','TSL-4: [ms]','TSL-5: [ms]'};
            dlg_title = 'Enter Values';
            num_lines = 1;
            exp_def = {'1.0', '2.0', '4.0', '8.0', '16.0'};
            exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
        case 6
            prompt = {'TSL-1, [ms]:','TSL-2: [ms]','TSL-3: [ms]','TSL-4: [ms]','TSL-5: [ms]','TSL-6: [ms]'};
            dlg_title = 'Enter Values';
            num_lines = 1;
            exp_def = {'1.0', '2.0', '4.0', '8.0', '16.0', '32.0'};
            exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
        otherwise
            errordlg(lasterr,'Error: incorrect input data','modal');
            return;
    end
end
for i=1:length(exp_param)
    xdata(i) = str2num(cell2mat(exp_param(i)));
end
clear prompt dlg_title num_lines exp_def exp_param;


prompt = {'Enter number of parameters for fit procedure'};
dlg_title = 'Enter Value';
num_lines = 1;
exp_def = {'2'};
exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
num_fit_para = str2num(cell2mat(exp_param));
clear prompt dlg_title num_lines exp_def exp_param;

% start values for fit procedure
if (num_fit_para == 2)
    prompt = {'S_0','T1rho: [ms]'};
    dlg_title = 'Enter start parameters';
    num_lines = 1;
    exp_def = {'4000', '5.0'};
    exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
elseif (num_fit_para == 3)
    prompt = {'S_0','T1rho: [ms]','noise'};
    dlg_title = 'Enter start parameters';
    num_lines = 1;
    exp_def = {'4000', '5.0', '50'};
    exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
else
    errordlg(lasterr,'Error: incorrect input data','modal');
    uiwait;
    close all;
end
for i=1:length(exp_param)
    start_par(i) = str2num(cell2mat(exp_param(i)));
end
clear prompt dlg_title num_lines exp_def exp_param;


% T1 value for fit procedure
prompt = {'Enter T1 value of the tissue [ms]'};
dlg_title = 'Enter Value';
num_lines = 1;
exp_def = {'500'};
exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
T1 = str2num(cell2mat(exp_param));
clear prompt dlg_title num_lines exp_def exp_param;


%select ROI for image noise  
figure;
imshow(ima_data(:,:,1),[0 500], 'InitialMagnification','fit');
title('Select ROI for background noise calculation');
ima_noise = imcrop;
close;
noise_mean = mean2(ima_noise);
noise_std = std2(ima_noise);


% select ROI for mapping 
figure;
imshow(ima_data(:,:,1),[0 500], 'InitialMagnification','fit');
title('Crop images to the region of interest');
[ima_crop,roi_map] = imcrop;
close;
for i=1:ima_count
    ima_crop(:,:,i) = imcrop(ima_data(:,:,i),roi_map);        
end


% display croped images
figure('Name','Croped images');
for i=1:ima_count
    subplot(1,ima_count,i);subimage(ima_crop(:,:,i),[0 2000]);
    axis off;
end


lb = [];
ub = [];
options = optimset('LargeScale','off', 'Jacobian','off', 'Algorithm', {'levenberg-marquardt', 0.005}, 'Display','off', 'GradObj','off', 'DerivativeCheck','off','TolX',1e-25, 'TolFun', 1e-25, 'MaxFunEvals', 1e25);
scrsz = get(0,'ScreenSize');
nroi=0;
while (nroi<100)
    figure;
    imshow(ima_crop(:,:,1),[0 400], 'InitialMagnification','fit');
    title('Select ROI for T1rho calculation');
    ROI_mask=roipoly;close;
    if (isempty(ROI_mask))
        errordlg(lasterr,'Error: incorrect input data','modal');
        break;
    end;
    
    ROI_vec=find(ROI_mask~=0);
    for n=1:ima_count
        ima_tmp = ima_crop(:,:,n);
        ydata(n) = mean(ima_tmp(ROI_vec));
    end
    SST_tmp = (ydata - mean(ydata)).^2;
    SST = sum(SST_tmp);
    
    if (ydata(1) > noise_mean + 3*noise_std)
        if (num_fit_para == 2)
            [calc_par,resnorm,~,~,~]=lsqcurvefit('t1rho_fit_2par',start_par,xdata,ydata,lb,ub,options);      % for 2-parameter fit
            M0 = calc_par(1);
            T1rho = calc_par(2);
            Noise = noise_mean;
        elseif (num_fit_para == 3)
            [calc_par,resnorm,~,~,~]=lsqcurvefit('t1rho_fit_3par',start_par,xdata,ydata,lb,ub,options);     % for 3-parameter fit
            M0 = calc_par(1);
            T1rho = calc_par(2);
            Noise = calc_par(3);
        else
            errordlg(lasterr,'Error: incorrect input data','modal');
            uiwait;
            close all;
        end
        GoodnessOfFit_SSE = resnorm;
        GoodnessOfFit_Rsquare = 1-resnorm/SST;
        GoodnessOfFit_RsquareAdj = 1-resnorm*(ima_count-1)/(SST*(ima_count-length(calc_par)));
        GoodnessOfFit_RMSE = sqrt(resnorm/(ima_count-length(calc_par)));
    else
        T1rho = 0;
        M0 = 0;
        Noise = 0;
        GoodnessOfFit_resnorm = Inf;
        GoodnessOfFit_RMSE = Inf;
        GoodnessOfFit_Rsquare = 0;
        GoodnessOfFit_RsquareAdj = 0;
    end
    
    alpha_rad=alpha*pi/180.0; % [rad]
    
    t = 0:0.001:xdata(ima_count)+0.01;
    figure('Position',[10 scrsz(4)/3 scrsz(4)/2 scrsz(4)/2]);
   
    plot(xdata,ydata,'*');
    hold on
    plot(t, Noise + M0*sin(alpha_rad)*exp(-t/T1rho).*(1 - exp(-(TR-t)/T1))./(1 - exp(-t/T1rho).*exp(-(TR-t)/T1)*cos(alpha_rad)), '--r');
    legend( 'Data', ['T1_{\rho} = ' num2str(T1rho,'%5.2f') ' ms']);
    text(xdata(ima_count-1),ydata(1),['Goodness of Fit = ' num2str(GoodnessOfFit_Rsquare, '%6.4f')]);
    title('Data fitting');
    hold off
    
    nroi=nroi+1;
end

end