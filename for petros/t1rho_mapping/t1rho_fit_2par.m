function [F,J] = t1rho_fit_2par(x,xdata)
nx = length(xdata);
i=1:nx;

global noise_mean TR T1 alpha

alpha_rad = alpha*pi/180.0;  % [rad]

noise_value = noise_mean;

% fit function
F(i) = noise_value + x(1) * sin(alpha_rad) * exp(-xdata(i)/x(2)) .* (1-exp(-(TR-xdata(i))/T1)) ./ (1 - exp(-xdata(i)/x(2)) .* exp(-(TR-xdata(i))/T1) * cos(alpha_rad) );

% derivative for x1-variable: dF/dx(1)
Gx1(i) = sin(alpha_rad) * exp(-xdata(i)/x(2)) .* (1-exp(-(TR-xdata(i))/T1)) ./ (1 - exp(-xdata(i)/x(2)) .* exp(-(TR-xdata(i))/T1) * cos(alpha_rad) ); 

% derivative for x1-variable: dF/dx(2)
Gx2(i) = x(1) * sin(alpha_rad) * xdata(i) .* exp(-xdata(i)/x(2)) .* (1-exp(-(TR-xdata(i))/T1)) ./ (x(2)^2 .* (1 - exp(-xdata(i)/x(2)) .* exp(-(TR-xdata(i))/T1) * cos(alpha_rad) ).^2);


Gx = [Gx1(i); Gx2(i)];
if nargout > 1
    J = Gx.';
end