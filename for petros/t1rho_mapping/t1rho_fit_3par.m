function [F,J] = t1rho_fit_3par(x,xdata)
nx = length(xdata);
i=1:nx;

global TR T1 alpha

alpha_rad = alpha*pi/180.0;   % [rad]

% fit function
F(i) = x(3) + x(1) * sin(alpha_rad) * exp(-xdata(i)/x(2)) .* (1-exp(-(TR-xdata(i))/T1)) ./ (1 - exp(-xdata(i)/x(2)) .* exp(-(TR-xdata(i))/T1) * cos(alpha_rad) );

% derivative for x1-variable: dF/dx(1)
Gx1(i) = sin(alpha_rad) * exp(-xdata(i)/x(2)) .* (1-exp(-(TR-xdata(i))/T1)) ./ (1 - exp(-xdata(i)/x(2)) .* exp(-(TR-xdata(i))/T1) * cos(alpha_rad) );

% derivative for x2-variable: dF/dx(3)
Gx2(i) = x(1) * sin(alpha_rad) * xdata(i) .* exp(-xdata(i)/x(2)) .* (1-exp(-(TR-xdata(i))/T1)) ./ (x(2)^2 .* (1 - exp(-xdata(i)/x(2)) .* exp(-(TR-xdata(i))/T1) * cos(alpha_rad) ).^2);

% derivative for x3-variable: dF/dx(3)
Gx3(i) = 1;

Gx = [Gx1(i); Gx2(i); Gx3(i)];
if nargout > 1
    J = Gx.';
end