function t1rho_map
close all;
clear all;

global noise_mean TR T1 alpha

noise_mean = 0;
noise_std = 0;

% Retrieve first image from user directory
[fname, pname] = uigetfile( ...
     {  '*.*',  'All Files (*.*)'; ...
        '*.dcm','DICOM-files (*.dcm)'; ...
        '*.nema','NEMA-files (*.nema)'}, ...    
        'Open Image Files');

% if no file was chosen -> back
if fname==0
    return;
end

fname=lower(fname);   
cd(pname);

ima_count = 0;
% Retrieve all images from the directory
names=dir;
for n = 1:(length(names))
   if (names(n).isdir)
       continue;
   end;
   ima_count=ima_count+1;
   try
       info=dicominfo(names(n).name);
       ima_orig(:,:,ima_count)=dicomread(info);
   catch  
       ima_count=ima_count-1;
   end
end    

TR = info.RepetitionTime;
alpha = info.FlipAngle;
ima_data = double(ima_orig);

lin = size(ima_data,1);
col = size(ima_data,2);

% display all images
figure('Name','Original images');
for i=1:ima_count
    subplot(1,ima_count,i);subimage(ima_data(:,:,i),[0 2000]);
    axis off;
end

if (ima_count < 3)
    errordlg(lasterr,'Error: incorrect input data','modal');
    return;
else
    switch ima_count
        case 3
            prompt = {'TSL-1, [ms]:','TSL-2: [ms]','TSL-3: [ms]'};
            dlg_title = 'Enter Values';
            num_lines = 1;
            exp_def = {'1.0', '2.0', '4.0'};
            exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
        case 4
            prompt = {'TSL-1, [ms]:','TSL-2: [ms]','TSL-3: [ms]','TSL-4: [ms]'};
            dlg_title = 'Enter Values';
            num_lines = 1;
            exp_def = {'1.0', '2.0', '4.0', '8.0'};
            exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
        case 5
            prompt = {'TSL-1, [ms]:','TSL-2: [ms]','TSL-3: [ms]','TSL-4: [ms]','TSL-5: [ms]'};
            dlg_title = 'Enter Values';
            num_lines = 1;
            exp_def = {'1.0', '2.0', '4.0', '8.0', '16.0'};
            exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
        case 6
            prompt = {'TSL-1, [ms]:','TSL-2: [ms]','TSL-3: [ms]','TSL-4: [ms]','TSL-5: [ms]','TSL-6: [ms]'};
            dlg_title = 'Enter Values';
            num_lines = 1;
            exp_def = {'1.0', '2.0', '4.0', '8.0', '16.0', '32.0'};
            exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
        otherwise
            errordlg(lasterr,'Error: incorrect input data','modal');
            return;
    end
end
for i=1:length(exp_param)
    xdata(i) = str2num(cell2mat(exp_param(i)));
end
clear prompt dlg_title num_lines exp_def exp_param;


prompt = {'Enter number of parameters for fit procedure'};
dlg_title = 'Enter Value';
num_lines = 1;
exp_def = {'2'};
exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
num_fit_para = str2num(cell2mat(exp_param));
clear prompt dlg_title num_lines exp_def exp_param;

% start values for fit procedure
if (num_fit_para == 2)
    prompt = {'S_0','T1rho: [ms]'};
    dlg_title = 'Enter start parameters';
    num_lines = 1;
    exp_def = {'4000', '5.0'};
    exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
elseif (num_fit_para == 3)
    prompt = {'S_0','T1rho: [ms]','noise'};
    dlg_title = 'Enter start parameters';
    num_lines = 1;
    exp_def = {'4000', '5.0', '50'};
    exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
else
    errordlg(lasterr,'Error: incorrect input data','modal');
    uiwait;
    close all;
end
for i=1:length(exp_param)
    start_par(i) = str2num(cell2mat(exp_param(i)));
end
clear prompt dlg_title num_lines exp_def exp_param;


% T1 value for fit procedure
prompt = {'Enter T1 value of the tissue [ms]'};
dlg_title = 'Enter Value';
num_lines = 1;
exp_def = {'500'};
exp_param=inputdlg(prompt,dlg_title,num_lines,exp_def);
T1 = str2num(cell2mat(exp_param));
clear prompt dlg_title num_lines exp_def exp_param;


% select ROI for image noise  
figure;
imshow(ima_data(:,:,1),[0 500], 'InitialMagnification','fit');
title('Select ROI for background noise calculation');
ima_noise = imcrop;
close;
noise_mean = mean2(ima_noise);
noise_std = std2(ima_noise);


% select ROI for mapping 
figure;
imshow(ima_data(:,:,1),[0 500], 'InitialMagnification','fit');
title('Crop images for mapping');
[ima_crop,roi_map] = imcrop;
close;
for i=1:ima_count
    ima_crop(:,:,i) = imcrop(ima_data(:,:,i),roi_map);        
end

figure('Name','Croped images');
for i=1:ima_count
    subplot(1,ima_count,i);subimage(ima_crop(:,:,i),[0 500]);
    axis off;
end

lin_crop = ceil(roi_map(4));
col_crop = ceil(roi_map(3));

M0_map = zeros(lin_crop,col_crop);
T1rho_map = zeros(lin_crop,col_crop);
Noise_map = zeros(lin_crop,col_crop);

SST = zeros(lin_crop,col_crop);
GoodnessOfFit_SSE = Inf(lin_crop,col_crop);
GoodnessOfFit_RMSE = Inf(lin_crop,col_crop);
GoodnessOfFit_Rsquare = zeros(lin_crop,col_crop);
GoodnessOfFit_RsquareAdj = zeros(lin_crop,col_crop);

lb = [];
ub = [];
options = optimset('LargeScale','off', 'Jacobian','off', 'Algorithm', {'levenberg-marquardt', 0.005}, 'Display','off', 'GradObj','off', 'DerivativeCheck','off','TolX',1e-25, 'TolFun', 1e-25, 'MaxFunEvals', 1e25);
wait = timebar('Computation in progress...','Wait...');
for i=1:lin_crop;
    for j=1:col_crop;                
        for m=1:ima_count
            ydata(m) = ima_crop(i,j,m);                                                  
        end
        % SST - sum of squares about the mean
        SST_tmp = (ydata - mean(ydata)).^2;
        SST(i,j) = sum(SST_tmp);
        clear SST_tmp;

        if (ydata(1) > (noise_mean + 3*noise_std))            
            if (num_fit_para == 2)
                [calc_par,resnorm,~,~,output]=lsqcurvefit('t1rho_fit_2par',start_par,xdata,ydata,lb,ub,options);           
                M0_map(i,j) = calc_par(1);
                T1rho_map(i,j) = calc_par(2);
                Noise_map(i,j) = noise_mean;
                GoodnessOfFit_SSE(i,j) = resnorm; 
            elseif (num_fit_para == 3)
                [calc_par,resnorm,~,~,output]=lsqcurvefit('t1rho_fit_3par',start_par,xdata,ydata,lb,ub,options);
                M0_map(i,j) = calc_par(1);
                T1rho_map(i,j) = calc_par(2);
                Noise_map(i,j) = calc_par(3);
                GoodnessOfFit_SSE(i,j) = resnorm;
            else
                errordlg(lasterr,'Error: incorrect input data','modal');
                uiwait;
                close all;
            end
        end
        timebar(wait,((i-1)*col_crop+j)/(lin_crop*col_crop));            
    end
end
close(wait);                                       


GoodnessOfFit_Rsquare = 1-GoodnessOfFit_SSE./SST;
GoodnessOfFit_RsquareAdj = 1-(ima_count-1)*GoodnessOfFit_SSE./(SST*(ima_count-length(calc_par)));
GoodnessOfFit_RMSE = sqrt(GoodnessOfFit_SSE/(ima_count-length(calc_par)));

T1rho_map(GoodnessOfFit_Rsquare>1)=0;
T1rho_map(GoodnessOfFit_Rsquare<0.5)=0;
T1rho_map(T1rho_map<0)=0;
T1rho_map(T1rho_map>1000)=0;

figure('Name', 'T1rho Map');    
imshow(T1rho_map,[0 80], 'InitialMagnification','fit');  
title('T1rho map');
colorbar; colormap(jet);

figure('Name', 'Goodness of fit');  
title('Goodness of Fit');
imshow(GoodnessOfFit_Rsquare,[0 1], 'InitialMagnification','fit');   
colorbar; colormap(jet);

[fname1,pname1] = uiputfile('*.mat','Save T1rho Map');
%falls kein file ausgew�hlt -> zur�ck
if fname1==0
    return;
end
cd(pname1);
save(fname1, 'T1rho_map', 'GoodnessOfFit_Rsquare');


% %F�r ROI-Analyse
% alpha_rad=alpha*pi/180.0; % [rad]
% t = 0:0.001:xdata(ima_count)+0.01;
% scrsz = get(0,'ScreenSize');
% q=1;
% while q < 100
%     figure;
%     imshow(ima_crop(:,:,1),[0 2000], 'InitialMagnification', 'fit');
%     title('Select ROI for T1rho calculation');    
%     roi_mask = roipoly; close;
%     if (isempty(roi_mask))
%         errordlg(lasterr,'Error: incorrect input data','modal');
%         break;
%     end;
%     roi_vec = find(roi_mask);
%     
%     for p=1:ima_count
%         ima_tmp = ima_crop(:,:,p);
%         roi_mean(p) = mean(ima_tmp(roi_vec));
%     end
%     SST_tmp = (roi_mean - mean(roi_mean)).^2;
%     SST = sum(SST_tmp);
% 
%     noise_tmp = mean(Noise_map(roi_vec));
%     m0_tmp = mean(M0_map(roi_vec));
%     t1rho_tmp = mean(T1rho_map(roi_vec));
%     
%     GoodnessOfFit_ROI_SSE = mean(GoodnessOfFit_SSE(roi_vec));
%     GoodnessOfFit_ROI_Rsquare = mean(GoodnessOfFit_Rsquare(roi_vec));
%     GoodnessOfFit_ROI_RsquareAdj = mean(GoodnessOfFit_RsquareAdj(roi_vec));
%     GoodnessOfFit_ROI_RMSE =  mean(GoodnessOfFit_RMSE(roi_vec));
%     
%     if ((GoodnessOfFit_ROI_Rsquare > 1) || (GoodnessOfFit_ROI_Rsquare < 0.5))
%         noise_tmp = 0;
%         t1rho_tmp = 0;
%         m0_tmp = 0;
%     end
%     
%     figure('Position',[10 scrsz(4)/3 scrsz(4)/2 scrsz(4)/2]);
%     plot(xdata,squeeze(roi_mean(1:ima_count)),'*b','LineWidth',2);
%     hold on
%     plot(t, noise_tmp + m0_tmp*sin(alpha_rad)*exp(-t/t1rho_tmp).*(1 - exp(-(TR-t)/T1))./(1 - exp(-t/t1rho_tmp).*exp(-(TR-t)/T1)*cos(alpha_rad)), ':r');
%     xlabel('Spin lock duration (ms)');
%     ylabel('Signal intensity (a.u.)');
%     legend( 'Data', ['T1_{\rho} = ' num2str(t1rho_tmp,'%5.2f') ' ms'])
%     text(xdata(ima_count-1),roi_mean(1),['Goodness of Fit = ' num2str(GoodnessOfFit_ROI_Rsquare, '%6.4f')]);
%     title('Data fitting');
%     hold off
%     
%     clear roi_vec roi mask;
%     q=q+1;
% end


end