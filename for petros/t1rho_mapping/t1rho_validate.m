function t1rho_validate(data_dir)

global noise_mean TR T1 alpha

noise_mean = 0;
noise_std = 0;


% Retrieve all images from the directory
listings=dir(data_dir);

spinlocktimes = {'1000', '2000', '4000', '8000'};
dir_count = 0;

for i = 1:length(spinlocktimes)
    spinlocktime = spinlocktimes{i};
    
    for j = 1:(length(listings))
        listing = listings(j);
        
       if (listing.isdir && contains(listing.name, spinlocktime))
           data_dirs(dir_count + 1).path = [listing.folder '/' listing.name];
           data_dirs(dir_count + 1).spinlocktime = spinlocktime;
           
           local_listing = dir(data_dirs(dir_count + 1).path);
           files = local_listing(~([local_listing.isdir]));
           data_dirs(dir_count + 1).files = files;
           data_dirs(dir_count + 1).numberOfFiles = length(files);
           
           dir_count = dir_count + 1;
       end;
    end    
end

if (~isequal(data_dirs(:).numberOfFiles) ...
    || length(data_dirs) ~= 4)
    return;
end

%% Loop over all slices
ima_count = 0;
% for i = 1:data_dirs(1).numberOfFiles
for i = 1:1
    for j = 1:length(data_dirs)
        file_path = [data_dirs(j).files(i).folder '/' data_dirs(j).files(i).name]
        ima_count=ima_count+1;

        try
           info=dicominfo(file_path);
           ima_orig(:,:,ima_count)=dicomread(info);
        catch  
           ima_count=ima_count-1;
        end
    end
    


    if ima_count ~= 4
        error(['All 4 images could not be loaded, only ' num2str(ima_count) ' were loaded']);
    end

    %% Input data 
    TR = info.RepetitionTime;
    alpha = info.FlipAngle;
    ima_data = double(ima_orig);    
    xdata = [1.0, 2.0, 4.0, 8.0];
    num_fit_para = 3
    start_par = [4000, 5.0, 50];
    T1 = 500;

    lin = size(ima_data,1);
    col = size(ima_data,2);
    
    
    lb = [];
    ub = [];
 
    %% Output initialization
    M0_map = zeros(lin, col);
    T1rho_map = zeros(lin, col);
    Noise_map = zeros(lin, col);

    SST = zeros(lin, col);
    GoodnessOfFit_SSE = Inf(lin, col);
    GoodnessOfFit_RMSE = Inf(lin, col);
    GoodnessOfFit_Rsquare = zeros(lin, col);
    GoodnessOfFit_RsquareAdj = zeros(lin, col);

    options = optimset('LargeScale','off', 'Jacobian','off', 'Algorithm', {'levenberg-marquardt', 0.005}, 'Display','off', 'GradObj','off', 'DerivativeCheck','off','TolX',1e-25, 'TolFun', 1e-25, 'MaxFunEvals', 1e25);
    tic
    for i=1:lin
        ydata = [];
        calc_par = zeros(1,3);
        disp(['Line ', num2str(i) ' of ' num2str(lin)]);
        for j=1:col
            for m=1:ima_count
                ydata(m) = double(ima_orig(i,j,m));                                         
            end
            % SST - sum of squares about the mean
            SST_tmp = (ydata - mean(ydata)).^2;
            SST(i,j) = sum(SST_tmp);
            SST_tmp = NaN;
            [calc_par,resnorm,~,~,output]=lsqcurvefit('t1rho_fit_3par',start_par,xdata,ydata,lb,ub,options);
            M0_map(i,j) = calc_par(1);
            T1rho_map(i,j) = calc_par(2);
            Noise_map(i,j) = calc_par(3);
            GoodnessOfFit_SSE(i,j) = resnorm;
 
        end
    end 
    toc

    GoodnessOfFit_Rsquare = 1-GoodnessOfFit_SSE./SST;
    GoodnessOfFit_RsquareAdj = 1-(ima_count-1)*GoodnessOfFit_SSE./(SST*(ima_count-3));
    GoodnessOfFit_RMSE = sqrt(GoodnessOfFit_SSE/(ima_count-3));

    T1rho_map(GoodnessOfFit_Rsquare>1)=0;
    T1rho_map(GoodnessOfFit_Rsquare<0.5)=0;
    T1rho_map(T1rho_map<0)=0;
    T1rho_map(T1rho_map>1000)=0;

    figure('Name', 'T1rho Map');    
    imshow(T1rho_map,[0 80], 'InitialMagnification','fit');  
    title('T1rho map');
    colorbar; colormap(jet);
% 
%     figure('Name', 'Goodness of fit');  
%     title('Goodness of Fit');
%     imshow(GoodnessOfFit_Rsquare,[0 1], 'InitialMagnification','fit');   
%     colorbar; colormap(jet);
% 
%     [fname1,pname1] = uiputfile('*.mat','Save T1rho Map');
%     %falls kein file ausgew�hlt -> zur�ck
%     if fname1==0
%         return;
%     end
%     cd(pname1);
%     save(fname1, 'T1rho_map', 'GoodnessOfFit_Rsquare');


    % %F�r ROI-Analyse
    % alpha_rad=alpha*pi/180.0; % [rad]
    % t = 0:0.001:xdata(ima_count)+0.01;
    % scrsz = get(0,'ScreenSize');
    % q=1;
    % while q < 100
    %     figure;
    %     imshow(ima_crop(:,:,1),[0 2000], 'InitialMagnification', 'fit');
    %     title('Select ROI for T1rho calculation');    
    %     roi_mask = roipoly; close;
    %     if (isempty(roi_mask))
    %         errordlg(lasterr,'Error: incorrect input data','modal');
    %         break;
    %     end;
    %     roi_vec = find(roi_mask);
    %     
    %     for p=1:ima_count
    %         ima_tmp = ima_crop(:,:,p);
    %         roi_mean(p) = mean(ima_tmp(roi_vec));
    %     end
    %     SST_tmp = (roi_mean - mean(roi_mean)).^2;
    %     SST = sum(SST_tmp);
    % 
    %     noise_tmp = mean(Noise_map(roi_vec));
    %     m0_tmp = mean(M0_map(roi_vec));
    %     t1rho_tmp = mean(T1rho_map(roi_vec));
    %     
    %     GoodnessOfFit_ROI_SSE = mean(GoodnessOfFit_SSE(roi_vec));
    %     GoodnessOfFit_ROI_Rsquare = mean(GoodnessOfFit_Rsquare(roi_vec));
    %     GoodnessOfFit_ROI_RsquareAdj = mean(GoodnessOfFit_RsquareAdj(roi_vec));
    %     GoodnessOfFit_ROI_RMSE =  mean(GoodnessOfFit_RMSE(roi_vec));
    %     
    %     if ((GoodnessOfFit_ROI_Rsquare > 1) || (GoodnessOfFit_ROI_Rsquare < 0.5))
    %         noise_tmp = 0;
    %         t1rho_tmp = 0;
    %         m0_tmp = 0;
    %     end
    %     
    %     figure('Position',[10 scrsz(4)/3 scrsz(4)/2 scrsz(4)/2]);
    %     plot(xdata,squeeze(roi_mean(1:ima_count)),'*b','LineWidth',2);
    %     hold on
    %     plot(t, noise_tmp + m0_tmp*sin(alpha_rad)*exp(-t/t1rho_tmp).*(1 - exp(-(TR-t)/T1))./(1 - exp(-t/t1rho_tmp).*exp(-(TR-t)/T1)*cos(alpha_rad)), ':r');
    %     xlabel('Spin lock duration (ms)');
    %     ylabel('Signal intensity (a.u.)');
    %     legend( 'Data', ['T1_{\rho} = ' num2str(t1rho_tmp,'%5.2f') ' ms'])
    %     text(xdata(ima_count-1),roi_mean(1),['Goodness of Fit = ' num2str(GoodnessOfFit_ROI_Rsquare, '%6.4f')]);
    %     title('Data fitting');
    %     hold off
    %     
    %     clear roi_vec roi mask;
    %     q=q+1;
    % end
end

end